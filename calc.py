#!/usr/bin/python3

def sumar(num1, num2):
  return num1 + num2

def restar(num1, num2):
  return num1 - num2

def main():
  n1 = 1
  n2 = 2
  suma1 = sumar(n1, n2)
  print(f"La suma de {n1} y {n2} es {suma1}")

  n3 = 3
  n4 = 4
  suma2 = sumar(n3, n4)
  print(f"La suma de {n3} y {n4} es {suma2}")

  n5 = 5
  n6 = 6
  resta1 = restar(n5, n6)
  print(f"La resta de {n5} y {n6} es {resta1}")

  n7 = 7
  n8 = 8
  resta2 = restar(n7, n8)
  print(f"La resta de {n7} y {n8} es {resta2}")

if __name__ == "__main__":
  main()
